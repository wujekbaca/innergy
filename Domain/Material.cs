﻿using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Representation of material
    /// </summary>
    public class Material : IEqualityComparer<Material>
    {
        public string Id { get; private set; }
        public string Name { get; private set; }

        public Material(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public bool Equals(Material x, Material y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;

            return (x.Name == y.Name && 
                    x.Id == y.Id);
        }

        public int GetHashCode(Material obj)
        {
            return (obj.Id + obj.Name).GetHashCode();
        }
    }
}
