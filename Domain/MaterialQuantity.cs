﻿namespace Domain
{
    /// <summary>
    /// Aggregation of material with available quantity
    /// </summary>
    public class MaterialQuantity
    {
        public Material Material { get; private set; }
        public double Quantity { get; private set; }

        public MaterialQuantity(Material material)
        {
            this.Material = material;
            this.Quantity = 0.0;
        }

        public MaterialQuantity(Material material, double quantity)
        {
            this.Material = material;
            this.Quantity = quantity;
        }

        public void AddQuantity(double quantity)
        {
            this.Quantity += quantity;
        }
    }
}
