﻿using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Contains all information about currently available warehouses
    /// with stored material quantities which are being aggregated
    /// to inventories format
    /// </summary>
    public class WarehouseManager
    {
        public List<WarehouseInventory> Inventories { get; private set; }

        public WarehouseManager()
        {
            this.Inventories = new List<WarehouseInventory>();
        }

        public void AddToInventory(
            Warehouse warehouse,
            MaterialQuantity materialQuantity
        )
        {
            var inventory = this.Inventories
                .Find(inv => inv.Warehouse.Equals(inv.Warehouse, warehouse));

            if (inventory is null)
            {
                inventory = new WarehouseInventory(warehouse);
                this.Inventories.Add(inventory);
            }

            inventory.AddMaterialQuantity(materialQuantity);
        }
    }
}
