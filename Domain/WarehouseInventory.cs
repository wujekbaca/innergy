﻿using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Representation of single warehouse with available
    /// materials and their quantities
    /// </summary>
    public class WarehouseInventory
    {
        public Warehouse Warehouse { get; private set; }
        public List<MaterialQuantity> MaterialQuantities { get; private set; }

        public WarehouseInventory(Warehouse warehouse)
        {
            this.Warehouse = warehouse;
            this.MaterialQuantities = new List<MaterialQuantity>();
        }

        public void AddMaterialQuantity(MaterialQuantity materialQuantity)
        {
            var matQuantity = this.MaterialQuantities.Find(mq => 
                mq.Material.Equals(mq.Material, materialQuantity.Material));

            if (matQuantity is null)
            {
                matQuantity = new MaterialQuantity(materialQuantity.Material);
                this.MaterialQuantities.Add(matQuantity);
            }

            matQuantity.AddQuantity(materialQuantity.Quantity);
        }

        public double GetTotalMaterialQuantity()
        {
            double result = 0.0;
            MaterialQuantities.ForEach(mq => result += mq.Quantity);
            return result;
        }
    }
}
