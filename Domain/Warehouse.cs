﻿using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Contains basic information about warehouse
    /// </summary>
    public class Warehouse : IEqualityComparer<Warehouse>
    {
        public string Name { get; private set; }

        public Warehouse(string name)
        {
            this.Name = name;
        }

        public bool Equals(Warehouse x, Warehouse y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;

            return (x.Name == y.Name);
        }

        public int GetHashCode(Warehouse obj)
        {
            return Name.GetHashCode();
        }
    }
}
