﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Services.Data;
using Services.Dto;

namespace Services
{
    public class MaterialImportService : IMaterialImportService
    {
        private readonly WarehouseManager _warehouseManager;

        public MaterialImportService()
        {
            _warehouseManager = new WarehouseManager();
        }
        
        public void ImportMaterials(List<string> stdInput)
        {
            List<ParseResult> parseResults = ParseData(stdInput);
            // add parsed data to warehouse manager 
            AddResultsToInventory(parseResults);
        }
        public List<string> GetMaterialsData()
        {
            List<string> result = new List<string>();
            // order by quantity then name
            var orderedInventories = _warehouseManager.Inventories
                .OrderByDescending(inventory => inventory.GetTotalMaterialQuantity())
                .ThenByDescending(inventory => inventory.Warehouse.Name);
            // add items to result
            foreach (var inventory in orderedInventories)
            {
                // warehouse summary
                result.Add(inventory.Warehouse.Name + " (total " + inventory.GetTotalMaterialQuantity() + ")");
                // materials
                var orderedMaterials = inventory.MaterialQuantities.OrderBy(mq => mq.Material.Id);
                foreach (var material in orderedMaterials)
                {
                    result.Add(material.Material.Id + ": " + material.Quantity);
                }
                // empty row
                result.Add("");
            }
            return result;
        }

        private void AddResultsToInventory(List<ParseResult> parseResults)
        {
            parseResults.ForEach(pr => pr.WarehouseQuantities.ForEach(
                wq => _warehouseManager.AddToInventory(
                    new Warehouse(wq.WarehouseName),
                    new MaterialQuantity(
                        new Material(pr.MaterialId, pr.MaterialName),
                        wq.Quantity
                    )
                )
            ));
        }

        private static List<ParseResult> ParseData(List<string> stdInput)
        {
            List<ParseResult> parseResults = new List<ParseResult>();
            // ignore rows by specified rule
            var filteredInput = stdInput
                .Where(input => !DataFilter.IsIgnored(input));
            foreach (var row in filteredInput)
            {
                parseResults.Add(Parser.ParseRow(row));
            }
            return parseResults;
        }
    }
}
