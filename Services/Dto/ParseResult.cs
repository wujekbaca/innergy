﻿using System.Collections.Generic;

namespace Services.Dto
{
    public class ParseResult
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public List<WarehouseQuantity> WarehouseQuantities { get; set; }

        public ParseResult()
        {
            this.WarehouseQuantities = new List<WarehouseQuantity>();
        }
    }

    public class WarehouseQuantity
    {
        public string WarehouseName { get; set; }
        public double Quantity { get; set; }

        public WarehouseQuantity(
            string warehouseName,
            double quantity)
        {
            this.WarehouseName = warehouseName;
            this.Quantity = quantity;
        }
    }
}
