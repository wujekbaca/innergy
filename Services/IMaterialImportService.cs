﻿using System.Collections.Generic;

namespace Services
{
    public interface IMaterialImportService
    {
        void ImportMaterials(List<string> stdInput);
        List<string> GetMaterialsData();
    }
}
