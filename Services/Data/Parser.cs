﻿using Services.Dto;

namespace Services.Data
{
    /// <summary>
    /// sample input:
    /// Nazwa materiału;
    /// ID Materiału;
    /// Magazyn,Ilość|Magazyn,Ilość|Magazyn,Ilość
    /// </summary>
    public static class Parser
    {
        public static ParseResult ParseRow(string input)
        {
            var result = new ParseResult();
            // separate input sections
            string[] sections = input.Split(';');
            // check if all sections are included
            if (sections.Length != 3)
                return result;
            // section one contains material name
            result.MaterialName = sections[0];
            // section two contains material id
            result.MaterialId = sections[1];
            // section three contains data about warehouse quantities 
            string[] warehouseQuantities = sections[2].Split('|');
            foreach (var warehouseQuantity in warehouseQuantities)
            {
                string[] warehouseQuantitySplit = warehouseQuantity
                    .Split(',');
                result.WarehouseQuantities.Add(new WarehouseQuantity(
                    warehouseQuantitySplit[0],
                    double.Parse(warehouseQuantitySplit[1])
                    ));
            }
            // return parsed data
            return result;
        }
    }
}
