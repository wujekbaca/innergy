﻿namespace Services.Data
{
    public static class DataFilter
    {
        public static bool IsIgnored(string input)
        {
            return input[0] == '#';
        }
    }
}
