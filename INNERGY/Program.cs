﻿using System;
using System.Collections.Generic;
using Services;
using static System.String;

namespace INNERGY
{
    class Program
    {
        static void Main(string[] args)
        {
            // collect input
            List<string> data = new List<string>();
            while (true)
            {
                 var input = Console.ReadLine();
                 if (!IsNullOrEmpty(input))
                 {
                     data.Add(input);
                     continue;
                 }
                 break;
            };
            // process data
            MaterialImportService materialImport = new MaterialImportService();
            materialImport.ImportMaterials(data);
            // show results
            var result = materialImport.GetMaterialsData();
            result.ForEach(Console.WriteLine);
        }
    }
}
